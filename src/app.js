'use strict';

let dm 			= require('./data_manager'),
	natural 	= require('natural'),
	classifier 	= new natural.BayesClassifier();

let app = {
	previous_row: null,
	processRow: (row) => {
		if (app.modifiers.skipFirstRow()) { return app.previous_row = row; }
		if (app.modifiers.skipEmptyValue(row) || app.modifiers.skipEmptyDesc(row)) { return };

		let delta = Math.abs(app.getDelta(row));
		let factors = app.getFactors(row);

		let trainingClass = app.getClass(app.getDelta(row));
		classifier.addDocument(factors, trainingClass);

		app.previous_row = row;
	},
	processingComplete: () => {
		classifier.train();
	},
	getClass: (value) => {
		if (value <= -2)				return 'very_very_bad';
		if (value <= -1 && value > -2)	return 'very_bad';
		if (value <= 0 && value > -1)	return 'bad';
		if (value >= 0 && value < 1)	return 'good';
		if (value >= 1 && value < 2)	return 'very_good';
		if (value >= 2)					return 'very_very_good';
	},
	getDelta	: row => (parseFloat(app.previous_row[1]) - parseFloat(row[1])).toFixed(3),
	getFactors	: row => row[2].split(' '),

	modifiers: {
		skipEmptyValue: row => row[1] === '',
		skipEmptyDesc: row => row[2] === '',
		skipFirstRow: () => app.previous_row === null,
	}
};

dm.readTrainingSet('demo', app.processRow, app.processingComplete);
classifier.events.on('doneTraining', function () {
	classifier.save('classifier.json', function(err, classifier) {
	    let test = classifier.classify('bagel');
	    console.log(test);
	});
});