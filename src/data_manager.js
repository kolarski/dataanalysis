'use strict';
let csv = require('fast-csv'),
	fs 	= require('fs');

let dm = {
	readTrainingSet: (training_set, onData, onEnd, transform) => {

		var stream = fs.createReadStream(`data/${training_set}.csv`);
 	
		csv
		 .fromStream(stream)
		 .transform(data => {
		 	if (typeof transform === 'function') {
		 		return transform(data);
		 	}
		 	return data;
		 })
		 .on("data", data => {
		 	if (typeof onData === 'function') {
		 		return onData(data);
		 	}
		 	throw new Error('Please supply onData handler');
		 })
		 .on("end", () => {
		 	if (typeof onEnd === 'function') {
		 		return onEnd();
		 	}
		 	throw new Error('Please supply onEnd handler');
		 });
	}
};

module.exports = dm;